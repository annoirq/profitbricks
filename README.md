# Instructions to run.
'Fine-Grain Locking Microservice' is based on Spring Boot.
So, just open console and type: `mvn spring-boot:run`

# Design approach used
Microservice is based on Spring Boot, so it is already included embedded webserver
and allowing parallel execution.

This application working as proxy with single end-point:

POST http://localhost:8080/locking/limiter/process
Header: Content-Type=application/json
```json
{
	"action": "CREATE",
	"itemType": "DATACENTER"
}
```

If you client will sent bad request, then response will be:
```json
{
    "timestamp": "2018-10-22T08:56:10.205+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Bad request",
    "path": "/locking/limiter/process"
}
```

Also, I've added validation (ValidationService, ExceptionHandlerController), for example follow json is not allowed:
```json
{
	"action": "CREATE"
}
```
Response:
```json
{
    "timestamp": "2018-10-22T08:57:40.507+0000",
    "status": 500,
    "error": "Internal Server Error",
    "message": "Request is not valid",
    "path": "/locking/limiter/process"
}
```

If request is ok, then proxy uses locking-mechanism & send request to mockedServer (RemoteFacade calls it).
MockedServer executes any request in 5 second.

All logic is in GrainedLocking.java
Application knows how all items: servers, datacenters, storages depends on each other (itemsDependency).
I've used Set of Trees (or Tree, with null-root data structure). I think it is perfectly fit here.
At the beginning itemsDependency is empty, and then depends on requests, it is being populated.


- **Reliability:** In the future this tree can be stored in Redis, so we can persist it to the disk.
- **Fault Tolerance:** Client-proxy (is very high, Validation is preventing from errors),
Proxy-MockedServer (My assumption, that MockedServer always responding with correct answer).
- **Scalability:** Datacenters are not dependend on each other, so per 1 datacenter can be deployed 1 instance of this app
Also, splitting of the tree will increase searching, deleting, adding operations to the tree.
- **Throughput:** Very high, as it's non-blocking. As soon as all nodes, which request is working on will be available, it will
request imediately will be processed.
- **Thread-safety:** safe.


There are several test-cases:
1.
```json
{
	"action": "CREATE",
	"itemType": "DATACENTER"
}
```
(or "SERVER", "STORAGE")


When client want to create new node, it should not be blocked at the beginning.
MockedServer responding like this:
```json
{
    "message": "Request was succesfully processed with mocked backend",
    "newlyCreatedId": 2
}
```

This node (id=2, itemType="DATACENTER") should be populated to the tree, and should be locked.
But it is fast operation, as it just adding node to the set O(1). Troughput is very high.

2.
```json
{
	"action": "DELETE",
	"itemType": "SERVER",
	"itemId": 4
}
```
(or "DATACENTER", "STORAGE") (or "UPDATE", "MAKESNAPSHOT")
First, I'm finding item, then in the loop:
locking, checking if all dependent nodes unlocked, unlocking.
Once all nodes will be unlocked, app will mark all needed nodes as locked.
So, for other threads condition 'Locked' will be true, if they want to use same nodes.

This is called non-blocking acquiring lock. It's working fast, and used in Java 5 java.util.concurrent library
in many classes. For example in the loop HashMap tries to process CompareAndSet, or CompareAndSwap.
Searching is O(log(n)), but if tree will be so big, so it can be splitted by datacenters.

After acquiring lock, mocked server responding 5 seconds, but itemsDependency is free.
After some time mocked server responding, and thread again need to acquire lock.. and then
case DELETE: delete nodes, case UPDATE, MAKESNAPSHOT mark nodes as unlocked... and then unlock tree.

3.
```json
{
	"action": "ATTACH",
	"itemType": "STORAGE",
	"itemId": 6,
	"withAttachToServerId": 7
}
```

Same with previous, but here we need to lock 2 items:
"STORAGE, 6" & "SERVER, 7".
And after everything will be finished, migrate "STORAGE, 6" under dependency "SERVER, 7"


# Proposal	(if	any)	for	future	extension	and	improvements
As I said previously, use Redis, so we can persist itemsDependency for reliability.
If this tree will be big, split it by datacenters, as they are not dependent on each other.
I will be increase speed of all operations with tree & will increase thoughput (as locking time spot will be smaller).

Also, about fault tolerance. If mockedserver will respond not correctly, I'm not handled exceptions.
So, here can be inconsistency. For example if it will not send id, then I cannot populate itemsDependency.
But this can be easely improved of course.


I don't see any other week points for now. Everything will work fine.
Also I see the depth of set of trees is very small (max=3), DATACENTER-STORAGE-SERVER.
So everything will work fast.

# High	level approach for testing.
I've added few tests in GrainedLockingTest. Just run it.

**nonBlockingTest**
 3 CREATE requests (each working 5 seconds). Finally all was processed in 5 seconds.
 That means, all request was not blocked.

**nonBlockingTest2**
  before testing, created 2 datacenters.
  Then 2 UPDATE requests, which tries to update non-dependent datacenters.
  Everything was finished in 5 seconds.
  That means, all request was not blocked.

**fullyBlockingTest**
    created 1 datacenter.
    Then 3 UPDATE requests, which tries to update same datacenter.
    Time execution more then 15 seconds.
    It means, that all requests was processed one by one.

 I didn't tested complex scenarios. I hope you will forgive me this.

 Or, you can test from POSTMAN. Please see above examples, how to call it. 

# Libraries: 
this is just Sprint Boot. Nothing Special.