package com.berlin.profitbricks.services;

import com.berlin.profitbricks.exceptions.ProfitBricksException;
import com.berlin.profitbricks.models.LimitterRequest;
import org.springframework.stereotype.Service;


@Service
public class ValidationService {
    private final static String REQUEST_IS_NOT_VALID = "Request is not valid";

    public void validateRequest(LimitterRequest request) {
        try {
            if (null == request.getAction()) throw new Exception();

            switch (request.getAction()) {
                case CREATE:
                    if (null == request.getItemType()) throw new Exception();
                    break;

                case UPDATE:
                    if (null == request.getItemId() || null == request.getItemType()) throw new Exception();
                    break;

                case DELETE:
                    if (null == request.getItemId() || null == request.getItemType()) throw new Exception();
                    break;

                case MAKESNAPSHOT:
                    if (null == request.getItemId() || null == request.getItemType()) throw new Exception();
                    break;

                case ATTACH:
                    if (null == request.getItemId() || null == request.getItemType()
                            || null == request.getAttachToServerId()) throw new Exception();
                    break;
            }
        } catch (Exception e) {
            throw new ProfitBricksException(REQUEST_IS_NOT_VALID);
        }
    }
}
