package com.berlin.profitbricks.models;


public class MockedBackendResponse {
    private String message;
    private Long newlyCreatedId;

    public MockedBackendResponse(String message) {
        this.message = message;
    }

    public MockedBackendResponse(String message, Long newlyCreatedId) {
        this.message = message;
        this.newlyCreatedId = newlyCreatedId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getNewlyCreatedId() {
        return newlyCreatedId;
    }

    public void setNewlyCreatedId(Long newlyCreatedId) {
        this.newlyCreatedId = newlyCreatedId;
    }
}
