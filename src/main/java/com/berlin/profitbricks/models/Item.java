package com.berlin.profitbricks.models;


import com.berlin.profitbricks.enums.ItemType;

import java.util.HashSet;
import java.util.Set;

public class Item {

    private ItemType type;
    private long id;
    private Set<Item> dependencies = new HashSet<>();
    private boolean locked = false;

    public Item(ItemType type, long id) {
        this.type = type;
        this.id = id;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Item> getDependencies() {
        return dependencies;
    }

    public void setDependencies(Set<Item> dependencies) {
        this.dependencies = dependencies;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (id != item.id) return false;
        if (type != item.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
