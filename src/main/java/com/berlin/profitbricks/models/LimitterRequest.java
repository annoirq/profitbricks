package com.berlin.profitbricks.models;

import com.berlin.profitbricks.enums.Action;
import com.berlin.profitbricks.enums.ItemType;

public class LimitterRequest {

    private Long datacenterId;
    private ItemType itemType;
    private Long itemId;
    private Long attachToServerId;
    private Action action;

    public Long getDatacenterId() {
        return datacenterId;
    }

    public void setDatacenterId(Long datacenterId) {
        this.datacenterId = datacenterId;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getAttachToServerId() {
        return attachToServerId;
    }

    public void setAttachToServerId(Long attachToServerId) {
        this.attachToServerId = attachToServerId;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public LimitterRequest() {}

    public LimitterRequest (Builder builder) {
        this.datacenterId = builder.datacenterId;
        this.itemType = builder.itemType;
        this.itemId = builder.itemId;
        this.attachToServerId = builder.attachToServerId;
        this.action = builder.action;
    }

    public static class Builder {
        public Long datacenterId;
        public ItemType itemType;
        public Long itemId;
        public Long attachToServerId;
        public Action action;

        public Builder withDatacenterId(Long datacenterId) {
            this.datacenterId = datacenterId;
            return this;
        }

        public Builder withItemType(ItemType itemType) {
            this.itemType = itemType;
            return this;
        }

        public Builder withItemId(Long itemId) {
            this.itemId = itemId;
            return this;
        }

        public Builder withAttachToServerId(Long attachToServerId) {
            this.attachToServerId = attachToServerId;
            return this;
        }

        public Builder withAction(Action action) {
            this.action = action;
            return this;
        }

        public LimitterRequest build() {
            return new LimitterRequest(this);
        }
    }
}
