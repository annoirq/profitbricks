package com.berlin.profitbricks.helpers;

import com.berlin.profitbricks.enums.Action;
import com.berlin.profitbricks.enums.ItemType;
import com.berlin.profitbricks.facade.RemoteFacade;
import com.berlin.profitbricks.models.Item;
import com.berlin.profitbricks.models.LimitterRequest;
import com.berlin.profitbricks.models.MockedBackendResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class GrainedLocking {
    private final static Set<Item> itemsDependency = new HashSet<>();

    @Autowired
    private RemoteFacade remoteFacade;

    public MockedBackendResponse processRequest(LimitterRequest request) {
        if (request.getAction() == Action.CREATE) {
            MockedBackendResponse response = remoteFacade.processRequest(request);
            Item item = new Item(request.getItemType(), response.getNewlyCreatedId());
            synchronized (itemsDependency) {
                itemsDependency.add(item);
            }
            return response;
        }

        if (Arrays.asList(Action.UPDATE, Action.DELETE, Action.MAKESNAPSHOT).contains(request.getAction())) {
            Item item = findItemInsideTree(new Item(request.getItemType(), request.getItemId()));
            while (true) {
                synchronized (itemsDependency) {
                    if (!isItemLocked(item)) {
                        lockItem(item);
                        break;
                    }
                }
            }

            MockedBackendResponse response = remoteFacade.processRequest(request);
            synchronized (itemsDependency) {
                if (request.getAction() == Action.DELETE) {
                    deleteItem(item);
                } else {
                    unlockItem(item);
                }
            }

            return response;
        }

        if (request.getAction() == Action.ATTACH) {
            Item item1 = findItemInsideTree(new Item(request.getItemType(), request.getItemId()));
            Item item2 = findItemInsideTree(new Item(ItemType.SERVER, request.getAttachToServerId()));
            while (true) {
                synchronized (itemsDependency) {
                    if (!isItemLocked(item1) && !isItemLocked(item2)) {
                        lockItem(item1);
                        lockItem(item2);
                        break;
                    }
                }
            }
            MockedBackendResponse response = remoteFacade.processRequest(request);
            synchronized (itemsDependency) {
                attachToServer(item1, item2);
                unlockItem(item1);
                unlockItem(item2);
            }

            return response;
        }

        return null;
    }

    private void attachToServer(Item item, Item server) {
        itemsDependency.remove(item);
        server.getDependencies().add(item);
    }

    private void deleteItem(Item item) {
        if (itemsDependency.contains(item)) {
            itemsDependency.remove(item);
            return;
        }

        Item parent = getParent(item, itemsDependency);
        parent.getDependencies().remove(item);
    }

    private Item getParent(Item item, Set<Item> set) {
        Set<Item> newSet = new HashSet<>();
        for (Item it : set) {
            if (it.getDependencies().contains(item)) {
                return it;
            } else {
                newSet.addAll(it.getDependencies());
            }
        }

        return getParent(item, newSet);
    }

    private void unlockItem(Item item) {
        if (null == item) return;
        item.setLocked(false);
        item.getDependencies().forEach(this::unlockItem);
    }

    private void lockItem(Item item) {
        if (null == item) return;
        item.setLocked(true);
        item.getDependencies().forEach(this::lockItem);
    }

    private boolean isItemLocked(Item item) {
        if (null == item) return false;
        if (item.isLocked())  return true;

        for (Item dependency : item.getDependencies()) {
            if (isItemLocked(dependency)) {
                return true;
            }
        }

        return false;
    }

    private Item findItemInsideTree(Item itemSearch) {
        Queue<Item> queue = new LinkedList<>();
        queue.addAll(itemsDependency);
        while (!queue.isEmpty()) {
            Item item = queue.poll();
            if (item.equals(itemSearch)) {
                return item;
            } else {
                queue.addAll(item.getDependencies());
            }
        }

        return null;
    }
}
