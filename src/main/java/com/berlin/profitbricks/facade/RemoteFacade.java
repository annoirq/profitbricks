package com.berlin.profitbricks.facade;

import com.berlin.profitbricks.enums.Action;
import com.berlin.profitbricks.models.MockedBackendResponse;
import com.berlin.profitbricks.models.LimitterRequest;
import org.springframework.stereotype.Component;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class RemoteFacade {
    private final static String MOCKED_BACKEND_MESSAGE = "Request was succesfully processed with mocked backend";
    private final static AtomicLong randomId = new AtomicLong(1);
    private final static long TIME_EXECUTION = 5000l;

    /**
     * call Mocked Provisioning Backend
     */
    public MockedBackendResponse processRequest(LimitterRequest request) {
        try {
            Thread.sleep(TIME_EXECUTION);
        } catch (Exception ignored) {}

        if (request.getAction() == Action.CREATE) {
            return new MockedBackendResponse(MOCKED_BACKEND_MESSAGE, randomId.getAndIncrement());
        }

        return new MockedBackendResponse(MOCKED_BACKEND_MESSAGE);
    }
}
