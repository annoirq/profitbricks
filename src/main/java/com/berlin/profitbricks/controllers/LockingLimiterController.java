package com.berlin.profitbricks.controllers;

import com.berlin.profitbricks.helpers.GrainedLocking;
import com.berlin.profitbricks.models.MockedBackendResponse;
import com.berlin.profitbricks.models.LimitterRequest;
import com.berlin.profitbricks.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("locking/limiter")
public class LockingLimiterController {

    @Autowired
    private GrainedLocking grainedLocking;

    @Autowired
    private ValidationService validationService;

    @PostMapping(value = "/process", consumes = MediaType.APPLICATION_JSON_VALUE)
    public MockedBackendResponse process(@RequestBody LimitterRequest request) {
        validationService.validateRequest(request);
        return grainedLocking.processRequest(request);
    }
}
