package com.berlin.profitbricks.enums;

public enum ItemType {
    DATACENTER, SERVER, STORAGE
}
