package com.berlin.profitbricks.enums;

public enum Action {
    CREATE, UPDATE, DELETE, ATTACH, MAKESNAPSHOT
}
