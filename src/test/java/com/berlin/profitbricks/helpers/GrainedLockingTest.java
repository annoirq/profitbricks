package com.berlin.profitbricks.helpers;

import com.berlin.profitbricks.enums.Action;
import com.berlin.profitbricks.enums.ItemType;
import com.berlin.profitbricks.models.LimitterRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GrainedLockingTest {

    @Autowired
    private GrainedLocking grainedLocking;

    @Test
    public void nonBlockingTest() throws ExecutionException, InterruptedException {
        CompletableFuture<String> createDatacenter = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(createDatacenterRequest()).getMessage());
        CompletableFuture<String> createServer = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(createServerRequest()).getMessage());
        CompletableFuture<String> createStorage = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(createStorage()).getMessage());

        long begin = System.currentTimeMillis();
        createDatacenter.get();
        createServer.get();
        createStorage.get();
        long end = System.currentTimeMillis();
        Assert.assertTrue((end - begin) < 5050);
    }

    @Test
    public void nonBlockingTest2() throws ExecutionException, InterruptedException {
        grainedLocking.processRequest(createDatacenterRequest());
        grainedLocking.processRequest(createDatacenterRequest());

        CompletableFuture<String> updateDatacenterRequest1 = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(updateDatacenterRequest(1l)).getMessage());
        CompletableFuture<String> updateDatacenterRequest2 = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(updateDatacenterRequest(2l)).getMessage());

        long begin = System.currentTimeMillis();
        updateDatacenterRequest1.get();
        updateDatacenterRequest2.get();
        long end = System.currentTimeMillis();
        Assert.assertTrue((end - begin) < 5050);
    }

    @Test
    public void fullyBlockingTest() throws ExecutionException, InterruptedException {
        grainedLocking.processRequest(createDatacenterRequest());

        CompletableFuture<String> updateDatacenterRequest1 = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(updateDatacenterRequest(1l)).getMessage());
        CompletableFuture<String> updateDatacenterRequest2 = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(updateDatacenterRequest(1l)).getMessage());
        CompletableFuture<String> updateDatacenterRequest3 = CompletableFuture.supplyAsync(()
                -> grainedLocking.processRequest(updateDatacenterRequest(1l)).getMessage());

        long begin = System.currentTimeMillis();
        updateDatacenterRequest1.get();
        updateDatacenterRequest2.get();
        updateDatacenterRequest3.get();
        long end = System.currentTimeMillis();
        Assert.assertTrue((end - begin) > 15000);
    }

    /**
     * Below all possible requests
     */
    private LimitterRequest createDatacenterRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.CREATE)
                .withItemType(ItemType.DATACENTER)
                .build();
    }

    private LimitterRequest createServerRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.CREATE)
                .withItemType(ItemType.SERVER)
                .build();
    }

    private LimitterRequest createStorage() {
        return new LimitterRequest.Builder()
                .withAction(Action.CREATE)
                .withItemType(ItemType.STORAGE)
                .build();
    }

    private LimitterRequest updateDatacenterRequest(long id) {
        return new LimitterRequest.Builder()
                .withAction(Action.UPDATE)
                .withItemType(ItemType.DATACENTER)
                .withDatacenterId(id)
                .withItemId(id)
                .build();
    }

    private LimitterRequest updateServerRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.UPDATE)
                .withItemId(2l)
                .withItemType(ItemType.SERVER)
                .build();
    }

    private LimitterRequest updateStorageRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.UPDATE)
                .withItemId(3l)
                .withItemType(ItemType.STORAGE)
                .build();
    }

    private LimitterRequest deleteDatacenterRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.DELETE)
                .withItemId(4l)
                .withItemType(ItemType.DATACENTER)
                .build();
    }

    private LimitterRequest deleteServerRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.DELETE)
                .withItemId(5l)
                .withItemType(ItemType.SERVER)
                .build();
    }

    private LimitterRequest attachStorageToServerRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.ATTACH)
                .withItemId(6l)
                .withItemType(ItemType.STORAGE)
                .withAttachToServerId(7l)
                .build();
    }

    private LimitterRequest makeSnapshotStorageRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.MAKESNAPSHOT)
                .withItemId(8l)
                .withItemType(ItemType.STORAGE)
                .build();
    }

    private LimitterRequest makeSnapshotServerRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.MAKESNAPSHOT)
                .withItemId(9l)
                .withItemType(ItemType.SERVER)
                .build();
    }

    private LimitterRequest makeSnapshotDatacenterRequest() {
        return new LimitterRequest.Builder()
                .withAction(Action.MAKESNAPSHOT)
                .withItemId(10l)
                .withItemType(ItemType.DATACENTER)
                .build();
    }
}
